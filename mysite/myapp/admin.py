# -*- coding: utf-8 -*-
 
from django.contrib import admin
from .models import Categories
 
 
class CategoriesAdmin(admin.ModelAdmin):
    list_filter = ['category']
    ordering = ['category']
 
admin.site.register(Categories, CategoriesAdmin)