from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
 
class Categories(models.Model):
    category_id = models.AutoField(primary_key=True)
    category = models.CharField(verbose_name=_('Category'), help_text=_('Add a Category like House, Apartament'), max_length=100)
 
    def __unicode__(self):
        return self.category
 
    class Meta:
        app_label = 'myapp'
        db_table = 'categories'
        verbose_name = _('category')
        verbose_name_plural = _('categories')